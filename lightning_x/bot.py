"""
Lightning-X
Copyright (C) 2021 LightSage

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation at version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import asyncio

from nio import AsyncClient, LoginError, RoomMessageText

from .callbacks import GCallbacks
from .config import Config


class LightningX:
    def __init__(self):
        self.config = Config('config.toml')

        self.client = AsyncClient(self.config['matrix']['homeserver'], self.config['matrix']['user']['id'])

        # Setup callbacks
        callback = GCallbacks(self)
        self.client.add_event_callback(callback.on_message, RoomMessageText)

    async def connect(self):
        resp = await self.client.login(self.config['matrix']['user']['password'])

        if type(resp) == LoginError:
            await self.close()

        await self.client.sync_forever(timeout=30000)

    async def close(self):
        await self.client.close()
